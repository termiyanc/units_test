<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Так как данных фильтров может быть много, используется POST-запрос, хотя никакой модификации данных не происходит
Route::post('/clients/filter', 'ClientsController@filter');

// По форме - GET-запросы, однако по сути - POST, так как происходит модификация
Route::post('/clients/attachTag/{client}/{tag}', 'ClientsController@attachTag');
Route::post('/clients/detachTag/{client}/{tag}', 'ClientsController@detachTag');
