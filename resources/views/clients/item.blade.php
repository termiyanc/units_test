<table class="clients-list-item">
    <tr>
        <th>Имя</th>
        <td>{{ $client->name }}</td>
    </tr>
    <tr>
        <th>Фамилия</th>
        <td>{{ $client->surname }}</td>
    </tr>
    <tr>
        <th>Номер телефона</th>
        <td>{{ $client->phone }}</td>
    </tr>
    <tr>
        <th>Дополнительный номер (кто проходит занятие)</th>
        <td>{{ $client->additional_phone }}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{ $client->email }}</td>
    </tr>
    <tr>
        <th>Ниша</th>
        <td>{{ $client->niche->value }}</td>
    </tr>
    <tr>
        <th>Роль</th>
        <td>{{ $client->role->value }}</td>
    </tr>
    <tr>
        <th>Штат (количество сотрудников)</th>
        <td>{{ $client->staff_size }}</td>
    </tr>
    <tr>
        <th>Пол</th>
        <td>{{ $client->sex->value }}</td>
    </tr>
    <tr>
        <th>Страна</th>
        <td>{{ $client->city->country->value }}</td>
    </tr>
    <tr>
        <th>Город</th>
        <td>{{ $client->city->value }}</td>
    </tr>
    <tr>
        <th>Средний оборот</th>
        <td>{{ $client->average_turnover->value }}</td>
    </tr>
    <tr>
        <th>Дата входа в воронку</th>
        <td>{{ $client->funnel_entered_at }}</td>
    </tr>
    <tr>
        <th>Аккаунт</th>
        <td>{{ $client->account }}</td>
    </tr>
    <tr>
        <th>Ссылка на таблицу (анкету)</th>
        <td><a href="{{ $client->link_to_profile }}">{{ $client->link_to_profile }}</a></td>
    </tr>
    <tr>
        <th>Статус аккаунта</th>
        <td>{{ $client->account_status->value }}</td>
    </tr>
    <tr>
        <th>Комментарий аккаунта</th>
        <td>{{ $client->account_comment }}</td>
    </tr>
    <tr>
        <th>На каком этапе клиент</th>
        <td>{{ $client->client_stage->value }}</td>
    </tr>
    <tr>
        <th>Когорты/теги</th>
        <td class="client-tags-container">
            <div class="client-tags" data-client-id="{{ $client->id }}">
                @include ('clients.tags_select', ['tags' => $client->tags, 'notAttachedTags' => $client->getNotAttachedTags()])
            </div>
            <div class="client-tags-loader hidden"></div>
        </td>
    </tr>
</table>

