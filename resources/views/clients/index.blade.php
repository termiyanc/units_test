<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ mix('/js/app.js') }}"></script>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
<form method="post" class="filters-form">
    <div class="filters-form-item filters-form-search">
        <div class="filters-form-item-title">Искать по имени и фамилии:</div>
        <div><input name="filters[search]"></div>
    </div>

    @foreach ($filters as $filterName => $filterDefinition)
        <ul class="filters-form-item filters-form-checkboxes">
            <li class="filters-form-item-title">{{ $filterDefinition['title'] }}</li>
            @foreach ($filterDefinition['values'] as $id => $title)
                <li><label><input type="checkbox" name="filters[checkboxes][{{ $filterName }}][]" value="{{ $id }}">{{ $title }}</label></li>
            @endforeach
        </ul>
    @endforeach

    <div class="filters-form-item filters-form-submit">
        <input type="submit" value="Применить фильтры">
    </div>
</form>

<div class="clients-list-container">
    <div class="clients-list hidden">
        @include('clients.list', ['clients' => $clients])
    </div>
    <div class="clients-list-loader"></div>
</div>
</body>
</html>
