@if ($notAttachedTags->count())
    <select class="client-tags-select">
        <option>Добавить тег</option>
        @foreach ($notAttachedTags as $notAttachedTag)
            <option value="{{ $notAttachedTag->id }}">{{ $notAttachedTag->value }}</option>
        @endforeach
    </select>
@endif

<ul class="client-tags-list">
    @foreach ($tags as $tag)
        <li>{{ $tag->value }} <span class="client-tags-remove" data-tag-id="{{ $tag->id }}">✕</span></li>
    @endforeach
</ul>
