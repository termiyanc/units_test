$(function () {
    const
        $filtersForm = $('.filters-form'),
        $filtersFormInputs = $('.filters-form input'),
        $clientsList = $('.clients-list'),
        $clientsListLoader = $('.clients-list-loader');

    // Отступ списка клиентов соответсвенно фиксированной форме фильтров слева
    $clientsList.parent('.clients-list-container').css('margin-left', $filtersForm.outerWidth());

    // Чтобы скрыть адаптацию списка, список выводится не сразу
    $clientsListLoader.addClass('hidden');
    $clientsList.removeClass('hidden');

    // Токен, необходимый для обращения к Laravel
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Скрытие/показ соответствующих элементов при начале отправки фильтров
    function onFiltersFormSubmitStarted () {
        $filtersFormInputs.prop('disabled', true);
        $clientsList.addClass('hidden');
        $clientsListLoader.removeClass('hidden');
    }

    // Скрытие/показ соответствующих элементов по окончании отправки фильтров
    function onFilterFormSubmitted() {
        $clientsListLoader.addClass('hidden');
        $clientsList.removeClass('hidden');
        $filtersFormInputs.prop('disabled', false);
    }

    // Отправка формы фильтров
    $filtersForm.on('submit', (e) => {
        e.preventDefault();

        // Так как поля формы в onFiltersFormSubmitStarted() примут состояние "disabled" и станут недоступны для получения,
        // следует получить данные формы предварительно
        const filtersFormData = $filtersForm.serialize();

        onFiltersFormSubmitStarted();
        $.ajax({
            url: '/api/clients/filter',
            method: 'post',
            data: filtersFormData,
            success(clientsList) {
                $clientsList.html(clientsList);
                initClientTags();
                onFilterFormSubmitted();
            },
            error() { // Простая заглушка для ошибок
                alert('Ошибка!');
                onFilterFormSubmitted();
            }
        });
    });

    // Инициализация обработки отдельного элемента тегов клиента
    function initClientTagsItem($clientTagsItem) {
        const $clientTagsLoader = $clientTagsItem.next('.client-tags-loader');

        function showClientTagsLoader() {
            $clientTagsLoader.removeClass('hidden');
            $clientTagsItem.addClass('hidden');
        }

        function hideClientTagsLoader() {
            $clientTagsLoader.addClass('hidden');
            $clientTagsItem.removeClass('hidden');
        }

        $clientTagsItem.find('.client-tags-remove').on('click', (e) => {
            const $removeTagButton = $(e.target);

            showClientTagsLoader();
            $.ajax({
                url: `/api/clients/detachTag/${$clientTagsItem.data('clientId')}/${$removeTagButton.data('tagId')}`,
                method: 'post',
                success(tagsSelect) {
                    $clientTagsItem.html(tagsSelect);
                    initClientTagsItem($clientTagsItem);
                    hideClientTagsLoader();
                },
                error() {
                    alert('Ошибка!');
                    hideClientTagsLoader();
                }
            });
        });

        $clientTagsItem.find('.client-tags-select').on('change', (e) => {
            let $tagsSelect = $(e.target);

            showClientTagsLoader();
            $.ajax({
                url: `/api/clients/attachTag/${$clientTagsItem.data('clientId')}/${$tagsSelect.val()}`,
                method: 'post',
                success(tagsSelect) {
                    $clientTagsItem.html(tagsSelect);
                    initClientTagsItem($clientTagsItem);
                    hideClientTagsLoader();
                },
                error() {
                    alert('Ошибка!');
                    hideClientTagsLoader();
                }
            })
        });
    }

    // Общая инициализация обработки тегов клиентов
    function initClientTags() {
        $('.client-tags').each((i, clientTagsItem) => initClientTagsItem($(clientTagsItem)));
    }

    initClientTags();
});
