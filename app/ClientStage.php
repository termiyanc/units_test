<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientStage extends Model
{
    /**
     * Связи
     */

    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
