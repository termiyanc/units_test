<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Niche extends Model
{
    /**
     * Связи
     */

    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
