<?php

namespace App\Http\Controllers;

use App\AccountStatus;
use App\AverageTurnover;
use App\City;
use App\Client;
use App\ClientStage;
use App\Niche;
use App\Tag;
use Illuminate\Http\Request;

class ClientsController extends Controller
{
    /**
     * Основной вывод клиентов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('clients/index')
            ->with('clients', Client::all())
            ->with('filters', [
                'city' => [
                    'title' => 'Город',
                    'values' => City::whereHas('clients')->pluck('value', 'id')
                ],
                'niche' => [
                    'title' => 'Ниша',
                    'values' => Niche::whereHas('clients')->pluck('value', 'id')
                ],
                'average_turnover' => [
                    'title' => 'Средний оборот',
                    'values' => AverageTurnover::whereHas('clients')->pluck('value', 'id')
                ],
                'account_status' => [
                    'title' => 'Статус аккаунта',
                    'values' => AccountStatus::whereHas('clients')->pluck('value', 'id')
                ],
                'tags' => [
                    'title' => 'Теги',
                    'values' => Tag::whereHas('clients')->pluck('value', 'id')
                ],
                'client_stage' => [
                    'title' => 'На каком этапе клиент',
                    'values' => ClientStage::whereHas('clients')->pluck('value', 'id')
                ],
            ]);
    }

    /**
     * Фильтрация клиентов
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter()
    {
        $filters = request()->post('filters');
        $clientsQuery = Client::query();

        // Поисковое поле
        $searchQuery = '%' . data_get($filters, 'search') . '%';
        $clientsQuery->where(function ($query) use ($searchQuery) {
            $query->where('name', 'like', $searchQuery)->orWhere('surname', 'like', $searchQuery);
        });

        // Чекбоксы
        foreach (data_get($filters, 'checkboxes', []) as $checkboxFilter => $checkboxFilterValues) {
            $clientsQuery->whereHas($checkboxFilter, function ($query) use ($checkboxFilterValues) {
                $query->whereIn('id', $checkboxFilterValues);
            });
        }

        return view('clients.list')->with('clients', $clientsQuery->get());
    }

    /**
     * Прикрепление тега к клиенту
     * @param Client $client
     * @param Tag $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function attachTag(Client $client, Tag $tag)
    {
        $client->tags()->attach($tag);
        return view('clients.tags_select')->with('tags', $client->tags)->with('notAttachedTags', $client->getNotAttachedTags());
    }

    /**
     * Открепление тега от клиента
     * @param Client $client
     * @param Tag $tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detachTag(Client $client, Tag $tag)
    {
        $client->tags()->detach($tag);
        return view('clients.tags_select')->with('tags', $client->tags)->with('notAttachedTags', $client->getNotAttachedTags());
    }
}
