<?php

namespace App;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * Связи
     */

    public function niche()
    {
        return $this->belongsTo(Niche::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function sex()
    {
        return $this->belongsTo(Sex::class);
    }
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function account_status()
    {
        return $this->belongsTo(AccountStatus::class);
    }

    public function average_turnover()
    {
        return $this->belongsTo(AverageTurnover::class);
    }

    public function client_stage()
    {
        return $this->belongsTo(ClientStage::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    /**
     * Не привязанные к клиенту теги
     * @return Collection
     */
    public function getNotAttachedTags()
    {
        return Tag::whereNotIn('id', $this->tags->pluck('id'))->get();
    }
}
