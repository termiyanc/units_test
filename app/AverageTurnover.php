<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AverageTurnover extends Model
{
    protected $table = 'average_turnover';

    /**
     * Связи
     */

    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
