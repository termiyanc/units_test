<?php

use App\AccountStatus;
use App\AverageTurnover;
use App\City;
use App\Client;
use App\ClientStage;
use App\Niche;
use App\Role;
use App\Sex;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Имена, фамилии ("1" - мужской, "2" - женский пол соответственно справочнику), комментарии для случайной генерации клиентов
        $names = [
            1 => [
                'Александр',
                'Дмитрий',
                'Павел',
                'Роман',
                'Харитон',
                'Сергей',
                'Антон',
                'Илья',
                'Фёдор',
                'Эдуард',
            ],
            2 => [
                'Алина',
                'Валентина',
                'Дарья',
                'Марина',
                'Ульяна',
                'Тамара',
                'Лариса',
                'Татьяна',
                'Любовь',
                'Людмила',
            ]
        ];

        $surnames = [
            1 => [
                'Иванов',
                'Петров',
                'Сидоров',
                'Кузнецов',
                'Воробьев',
                'Фетисов',
                'Пронин',
                'Лукашин',
                'Алексеенко',
                'Мишутин',
            ],
            2 => [
                'Максимова',
                'Красина',
                'Дьяченко',
                'Александрова',
                'Перфильева',
                'Макарова',
                'Шестова',
                'Курёхина',
                'Дягилева',
                'Самохвалина',
            ]
        ];

        $comments = [
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
            'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
            'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.',
            'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.',
            'Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.',
            'Et harum quidem rerum facilis est et expedita distinctio.',
            'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.',
            'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.',
        ];

        // Случайная генерация клиентов
        for ($i = 1; $i <= 50; $i++) {
            $client = new Client;

            // Справочные поля
            $client->sex()->associate(Sex::all()->random());
            $client->niche()->associate(Niche::all()->random());
            $client->role()->associate(Role::all()->random());
            $client->city()->associate(City::all()->random());
            $client->average_turnover()->associate(AverageTurnover::all()->random());
            $client->account_status()->associate(AccountStatus::all()->random());
            $client->client_stage()->associate(ClientStage::all()->random());

            // Поля непосредственных значений
            $client->name = $names[$client->sex_id][array_rand($names[$client->sex_id], 1)];
            $client->surname = $surnames[$client->sex_id][array_rand($surnames[$client->sex_id], 1)];
            $client->phone = '8' . mt_rand(1000000000, 9999999999);
            $client->additional_phone = '8' . mt_rand(1000000000, 9999999999);
            $client->email = $client->phone . '@mail.ru';
            $client->staff_size = mt_rand(1, 1000);
            $client->funnel_entered_at = Carbon::now()->subDays(mt_rand(1, 365));
            $client->account = $client->phone;
            $client->link_to_profile = 'http://profiles.ru/' . $client->phone;
            $client->account_comment = $comments[array_rand($comments, 1)];

            $client->save();

            // Так как теги привязываются к клиентам через промежуточную таблицу,
            // привязать тег возможно только к сохраненному клиенту
            $client->tags()->attach(Tag::all()->random());
        };
    }
}
