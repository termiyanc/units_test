<?php

use App\AccountStatus;
use App\AverageTurnover;
use App\City;
use App\ClientStage;
use App\Country;
use App\Niche;
use App\Role;
use App\Sex;
use App\Tag;
use Illuminate\Database\Seeder;

class ReferenceTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 5; $i++) {
            Niche::create(['value' => "Ниша $i"]);
        }

        Role::create(['value' => 'собственник']);
        Role::create(['value' => 'сотрудник']);

        Sex::create(['value' => 'мужской']);
        Sex::create(['value' => 'женский']);

        $russia = Country::create(['value' => 'Россия']);
        $ukraine = Country::create(['value' => 'Украина']);

        City::create(['value' => 'Москва', 'country_id' => $russia->id]);
        City::create(['value' => 'Рязань', 'country_id' => $russia->id]);

        City::create(['value' => 'Киев', 'country_id' => $ukraine->id]);
        City::create(['value' => 'Тернополь', 'country_id' => $ukraine->id]);

        AverageTurnover::create(['value' => '0 - 150']);
        AverageTurnover::create(['value' => '151 - 300']);
        AverageTurnover::create(['value' => '301 и больше']);

        AccountStatus::create(['value' => 'завершил пакет']);
        AccountStatus::create(['value' => 'прошел юнит']);
        AccountStatus::create(['value' => 'прошел диагностику']);

        ClientStage::create(['value' => 'начало пути']);
        ClientStage::create(['value' => 'полпути']);
        ClientStage::create(['value' => 'предпоследний']);
        ClientStage::create(['value' => 'лонг']);
        ClientStage::create(['value' => 'шот']);
        ClientStage::create(['value' => 'завершил']);
        ClientStage::create(['value' => 'входит']);

        for ($i = 1; $i <= 5; $i++) {
            Tag::create(['value' => "Тег $i"]);
        }
    }
}
