<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferenceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('niches', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('sex', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('countries', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('cities', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedTinyInteger('country_id');
            $table->string('value');
            $table->timestamps();

            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('average_turnover', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('account_statuses', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('client_stages', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->tinyIncrements('id');
            $table->string('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tags');
        Schema::drop('client_stages');
        Schema::drop('account_statuses');
        Schema::drop('average_turnover');
        Schema::drop('cities');
        Schema::drop('countries');
        Schema::drop('sex');
        Schema::drop('roles');
        Schema::drop('niches');
    }
}
