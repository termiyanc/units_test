<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Так как не обговорено иное, ни одно поле не допускает null-значения,
        // а также не создаются уникальные индексы на полях, кроме первичного ключа id
        Schema::create('clients', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->string('surname');
            $table->string('phone');
            $table->string('additional_phone');
            $table->string('email');
            $table->unsignedTinyInteger('niche_id');
            $table->unsignedTinyInteger('role_id');
            $table->unsignedSmallInteger('staff_size');
            $table->unsignedTinyInteger('sex_id');
            // $table->unsignedTinyInteger('country_id'); Данное поле не добавляется непосредственно в таблицу, так как его значение определяет следующее поле
            $table->unsignedSmallInteger('city_id');
            $table->unsignedTinyInteger('average_turnover_id');
            $table->date('funnel_entered_at');
            $table->string('account');
            $table->string('link_to_profile');
            $table->unsignedTinyInteger('account_status_id');
            $table->string('account_comment');
            $table->unsignedTinyInteger('client_stage_id');

            // Так как ни одно поле не допускают null-значения, для внешних ключей задается каскадное удаление
            $table->foreign('niche_id')->references('id')->on('niches')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sex_id')->references('id')->on('sex')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('city_id')->references('id')->on('cities')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('average_turnover_id')->references('id')->on('average_turnover')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('account_status_id')->references('id')->on('account_statuses')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('client_stage_id')->references('id')->on('client_stages')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clients');
    }
}
